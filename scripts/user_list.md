# User Rule #
| User-ID |	Usage |
| -------| -------|
| 1000-2000| for Operator|
| 2001-2800| for Data API related systems |
| 2801-3000| for Data Service related systems |
| 3001-4000| for IT related systems |
| 4001-5000| for Portal related systems |

# User List #
| User-ID |	User | System |
| -------| -------| -----------------------------------|
| 2001	| p2pas	  | P2P Assistant System|
| 2002	| ssas    | Session Signal Assistant System|
| 2003	| scsnms  | SCS Node Management System|
| 2004	| dms     | Device Management System|
| 2005	| udms    | User Device Management System|
| 2006	| dfms    | Device Firmware Management System|
| 2007	| dvms    | Device Vendor Management System|
| 2008	| uams    | User Account Management System|
| 2010	| ssacms  | Session Signal Assistant Cluster Management System|
| 2011	| p2pacms | P2P Assistant Cluster Management System|
| 2014	| uas     | User Assistant System|
| 2015	| albumebs| albume Box System|
| 2016	| tms     | Token Management System|
| 2018	| proxys  | Proxy System|
| 2019	| nms   | Notification Management System|
| 2020	| fsms   | File Storage Management System|
| 2021	| albumems   | albume Management System|
| 2022	| pms   | Payment Management System|
| 2023	| feedbackms   | Feedback Management System|
| 2024	| ams   | Album Management System|
| 2025	| opms   | Order Process Management System|
| 2026	| aas   | Album Assistant System|
| 2027	| urms   | User Recommend Management System |
| 2028	| msms   | Media Sharement Management System|
| 2029	| ucms   | User Contact Management System|
| 2030	| cpms   | Coupon Management System|
| 2031	| mams   | Marketing Activity Management System|
| 2032  | cfms   | Classfication Management System|
| 2033  | psms   | Print Service Management System|
| 2034  | nss   | Notification Schedule System|
| 2035  | msss   | Media Sharement Schedule System|
| 2801	| cbms   | Couchbase Management System|
| 3001  | dreams  | SVN&Trac|
| 3002  | fes     | File Exchange System|
| 4001	| crps   | CleverRock Portal System, www.clever-rock.com |
| 4002	| albumeps   | albume Portal System, www.albume.me |
| 4003	| cleverintechps   | Clever-In-Tech Portal System, www.cleverintech.com |
| 4004  | albumecnps | Portal System of www.albume.cn |