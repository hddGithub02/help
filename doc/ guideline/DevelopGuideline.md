# 服务器端开发规范

----
## 基本信息
* 版本
    * V1.0
* 制订人
    * 颜文远
* 密级
    * 公司内使用，请勿外传

## 总论
### 开发准则
* 大规模可扩展
* 24小时在线
* 高效率的开发
* 高效的资源利用
* 沟通
* 标准化

## 开发规范
### 环境管理
#### OS
* 操作系统统一采用CentOS6.x(当前统一使用的版本:Linux 2.6.18-8.el5)

#### 用户
* 用户分类
    * 普通用户
        * 用于操作人员，一个操作人员对应一个普通用户，**不允许出现多个人员共用一个普通用户的情况**
        * 所有的普通用户都加入normal组（gid为2001），根据需要决定是否加入wheel组
    * 程序用户
        * 用于应用系统，**一个应用系统对应一个程序用户**
        * 所有的程序用户都属于program组（gid为1001），原则上不加入任何其它的组
* 用户管理原则
    * 所有用户的id统一分配
    * 用户命名规则： **用户名只能由小写字母组成**，不能包含：数字，大写字母，符号
    * 权限管理
        * 不可直接通过root用户进行操作  
        * 通过sudo命令，实现超级用户的权限
        * 只有属于wheel组的用户，才拥有sudo的权限
    * 密码管理
        * 普通用户的密码，由相关使用者自己定，需要定期修改密码
        * 程序用户统一设置一个自动随机的密码，统一通过如下的方式切换到程序用户 ： sudo su - $USER

#### 软件
* 所有的系统软件，都必须通过yum安装，**杜绝手工编译安装**
* 例外的情况下，如果确实必须安装非标准软件，请将该软件安装到应用系统对应的$HOME下

#### 运行环境
* 3套环境分开：开发环境，测试环境，正式环境
    * 开发环境：开发人员用于开发，及代码的调试，使用本地搭建的虚拟机
    * 测试环境：用于系统间的联调，功能测试，性能测试
    * 正式环境：正式部署，用于提供正式服务,使用阿里云

### 部署管理
* mysql的安装配置原则
    * 数据库的root用户，只用于系统维护，应用系统不得使用root用户
    * 不同的应用系统，单独规划各自的mysql用户，并只赋予所需要的相应权限。
    * mysql用户的密码规则统一制定。密码规则为：倒写的用户名-5412。 如用户名user1,则对应的密码为1resu-5412
    * 不同系统所需要创建的数据库，由各个系统单独创建，使用如下的端口号：$userid*10
* php的安装配置原则
    * 所有系统，使用统一的php配置文件
    * 打开php错误日志，各个系统的错误日志记载到$HOME/log/php/error.log 中
* apache的安装配置原则
    * 尽可能使用虚拟主机，以方便多个服务灵活的共用同一台服务器。
    * apache的访问日志，统一采用combined格式
    * apache的日志，统一采用rotatelogs进行轮转
    * apache日志文件名称，统一为$HOME/log/apache/apache_nms_%Y%m%d.err.log及$HOME/log/apache/apache_nms_%Y%m%d.log
    * 缺省的http.conf文件，尽可能不做修改。
    * 不同的应用系统，各自使用自己的apache端口号：$userid*10+1
    * 不同的虚拟主机，直接通过不同的域名进行区分，apache的缺省主机不许使用
    * 不同的系统分别将各自对应的配置文件，由部署工具自动链接到apache的配置文件的Include目录下

### 应用系统开发
#### 原则
* 开发和部署的基本单位：应用系统。
* 每个应用系统，对应gitlab上的一个project，与应用系统相关的，都需要通过git进行管理，包括代码，维护脚本，crontab，软件配置文件（mysql/apache等)等
* 所有的与该系统相关的文件，**都放在程序用户对应的$HOME目录下**
* 每个系统可以使用 [$userid*10， $userid*10 + 9]范围内的端口（$userid表示应用系统账号的id）
    * $userid*10     : mysql端口
    * $userid*10 + 1 : apache端口（不使用80端口号的情况下）
    * $userid*10 + 2 : memcache端口
    * $userid*10 + 3 : redis端口
    * $userid*10 + 4 : postgresql端口
    * $userid*10 + 5 : pgbouncer端口
    * 其它           : 由应用系统自由分配
#### 目录规范
* $HOME目录下的统一目录结构</br>

    $HOME/  
    |----conf/   
    |----pear/   
	|----src/    
	|----data/   
	|----log/    
	|----www/    
	|----tmp/    

    * 说明：
        * conf：用于存放环境相关的配置文件
        * pear：本应用系统相关的pear软件
        * src：用于存放系统相关的代码
        * data：用于存放系统相关的数据，如mysql数据库，生成的文件等
        * log：用于存放系统的日志，如apache日志，运行日志等
        * www：用于存放 apache !DocumentRoot 对应的源代码的链接
        * tmp：用于存放系统的一些临时文件，以及一些手工生成的临时文件，临时文件是可被随时删除而不影响服务的
* $HOME/src目录下的统一目录结构
	
	$HOME/	
	|----src/	 
	|----|----CURRENT/	 
	|----|----releases/ 	
    |----|----|----master	

    * 说明：
        * CURRENT——链接，指向当前正在使用的代码版本:/$HOME/src/releases/master/src
            * 注意：CURRENT链接为当前使用的分支的唯一标示，所有的其它目录，都基于该链接
* $HOME/src/CURRENT目录下的统一目录结构

    $HOME/  
    |----src/    
	|----|----CURRENT/    
	|----|----|----config/ 	    
	|----|----|----etc/	    
	|----|----|----inc/	    
	|----|----|----web/	    
	|----|----|----scripts/	    
	|----|----|----maint/	    
	|----|----|----test/	    
    * 说明：
        * config——用于存放配置文件
        * etc——用于存放其它的与环境相关的文件
        * inc——用于存放内部的库的文件
        * web——用于存放apache文件的根目录，统一负责链接到 $HOME/www/htdocs
        * scripts——用于存放系统相关的运行脚本
        * maint——用于存放与本应用系统的运维相关的代码
        * test——用于存放与测试相关的代码
* $HOME/data目录下的统一目录结构

	$HOME/  
	|----data/   
	|----|----mysql/  
	|----|----... 

    * 说明：
        * mysql——应用系统对应的mysql的根目录
        * 本应用系统的其它数据目录，根据需要在data目录下创建
        * 如果某个数据文件目录需要被其它系统访问，可以通过chmod g+rwx 来进行权限设置（所有的程序用户都属于program组）
* $HOME/log目录下的统一目录结构

	$HOME/  
	|----log/    
	|----|----crontab/    
	|----|----php/    
	|----|----apache/   
	|----|----... 
    
    * 说明：
        * crontab——crontab任务运行相关的日志
        * php——php日志
        * apache——apache日志
        * 本系统的其它日志目录，根据需要在log目录下创建（原则上，log下只有一层子目录）
* $HOME/www目录下的统一目录结构

	$HOME/  
	|----www/    
	|----|----htdocs  

    * 说明：
        * htdocs——apache的!DocumentRoot路径。指向$HOME/src/CURRENT/web

* 多硬盘情况下的目录规范
    * 多硬盘的情况下，第二章硬盘开始，分别对应的分区为/da1,/da2...。 /daN下的对应的目录结构如下

	/daN/   
	|----$USER_1/    
	|----$USER_2/    
	|----... 

* 说明：
    * $USER_xxx为各个程序用户名
        * /daN/$USER_xxx下的路径结构与对应的程序用户下的/$HOME/下的目录结构保持一致
        * 如果使用了/daN存放数据或日志，则在相应的$HOME/下的目录建立链接，如：ln -s /daN/$USER_XXX/data/mysql $HOME/data/mysql

#### 应用系统维护规范
* 系统维护，由应用系统自身负责，包括：
    * 无用的数据与日志的定期清除
    * 重要数据和日志的备份 
* 数据备份规范：由应用系统根据自身逻辑，定期将需要备份的数据存放到 **/home/maint/data/backup/** 下
    * 文件名规则： $SYSNAME_$SUFFIX_$IP_$DATE.$EXT
        * $SYSNAME -> 系统名称
        * $SUFFIX  -> 任意，例如，data, mysql, table 等
        * $IP      -> 服务器 IP 去掉点后的串 (e.g. 192.168.1.1 -> 19216811)
        * $DATE    -> 一般应为 YYYYMMDD 格式
        * $EXT     -> 扩展名，一般应以 .tar.gz, .zip, .tar 等结束

### 相关资源
#### 服务器安装
* 标准CentOS6.x环境安装(包括阿里云与Vmware)
    * [标准CentOS6.x环境安装](scripts/setup_baseline_centos.sh)
* Amozon云下的CentOS6.x环境安装
    * [ami CentOS6.x环境安装](scripts/setup_baseline_ami.sh)

#### 库资源
* 公司内的Pear
    * [Clever Rock Pear](http://pear.taxicolor.org)
    * 说明：每台机器，应安装如下的基本pear包，以便进行相关的系统管理和运维

    sudo pear channel-discover pear.taxicolor.org   
    sudo pear install -f taxicolor/maint    
    sudo pear install -f taxicolor/pear_mgr 
