# 服务器端API规范

## API风格
* 面向对象

## 请求

### HTTP协议
* HTTPS
    * 安全性要求高的API
* HTTP
    * 安全性要求低而且对性能要求很高的API
    * 客户端不支持HTTPS的情况

### URL 路径

* URL 路径格式 : $PREFIX/$OBJ_CLASS/-/$ID_1![/-/$ID_2/-/...]
    * URL路径中的多个部分， 以 **/-/** 区隔
    * $OBJ_CLASS : 对象类型
    * $ID_1,$ID_2,... : 与对象相关的ID，用于唯一标识一个对象

### HTTP 方法

* GET : 用于读对象的API
* POST : 用于写对象的API
* DELETE : 用于删除对象的API

### HTTP 报文头
* 待补充

### HTTP 参数
* $_GET
    * 内容比较短
    * 希望记录到apache日志里面的参数（方便后续的统计或问题定位）
* $_POST
    * 内容较长
    * 与安全相关的参数

#### 标准参数

* 基本参数
    * op : 本API对应的操作的名称
        * 用于区隔使用相同HTTP方法的多个API
    * alt : 返回的内容的格式
        * json (default) : json 
        * xml : xml 
        * php : php serialize
        * dump : php dump
    * callback : jsonp的回调函数的名称, 只有在 alt=json 时才生效
    * invoke-mode : 调用本API的模式
        * debug : debug 模式(错误时，会返回详细的错误信息), 用于进行API的调试
        * normal (缺省): normal 模式(错误时，只会返回简单的错误信息)
    * api-ver : 本次调用所对应的API版本(格式 : $major.$minor.$patch)
    * err-report-mode : 错误报告的模式
        * silent : 在发生错误时，不返回错误相关的内容
        * normal (缺省): 在发生错误时，返回错误相关的内容
* 遍历相关的参数
    * start : 所需要的记录的起始位置，仅在与遍历相关的API中有效
        * 从0开始计数
    * count : 所需要的记录的条数，仅在与遍历相关的API中有效

## 返回

### HTTP 返回码

* 成功
    * HTTP返回码: 200 或 204 或 206
* 失败
    * HTTP返回码: 4xx 或 5xx

### 返回数据格式

#### 顶级格式（json格式）

```json
{
    "?version": "1.0",
    "?encoding": "utf-8",
    "information": {
        "@version": "1.0",
        "data": {
            "api": {
                "status": $STATUS,
                "spent-time": $SPENT_TIME
                "error": {
                    "@type": $ERROR_TYPE, 
                    "#t": $ERROR_INFO
                }
            }
            $CONTENT_DATA
        }
    }
}
```

* $STATUS : API的执行情况
    * SUCCESS : 成功
    * FAILED : 失败
* $SPENT_TIME : 本次API执行所花费的时间，单位为秒
* $ERROR_TYPE : 发生错误的类型，只在API执行失败的情况下有效
    * INPUT : 请求的内容存在错误
    * ...
* $ERROR_INFO : 发生错误的相关信息，只在API执行失败的情况下有效
* $CONTENT_DATA : 返回的具体内容，与API直接相关

#### $CONTENT_DATA 格式

##### Case 1 : 返回操作结果的信息

```json
"message" : $MESSAGE_DATA
```

* $MESSAGE_DATA : 具体的操作结果信息, 由个API各自具体定义

##### Case 2 : 返回对象记录的列表

```json
"traverse-info": {
    "total-count": $TOTAL_COUNT,
    "accessable-count": $ACCESSABLE_COUNT,
    "start": $START,
    "request-count": $REQUEST_COUNT
},
"record-list": {
    "@count": $RECORD_COUNT,
    "record": [
        {
            $RECORD_DATA
        }
    ]
},
```

* $TOTAL_COUNT : 符合条件的对象记录的总数量
* $ACCESSABLE_COUNT : 符合条件的对象记录中，能够通过本API中实际获取到的总数量
* $START : 返回的对象记录的起始位置
    * 从 0 开始计数
* $REQUEST_COUNT : 要求返回的对象记录的数量
* $RECORD_COUNT : 实际返回的对象记录的数量
* $RECORD_DATA : 具体的对象记录信息, 由个API各自具体定义

### 不同返回内容的格式转换规则 ###
* json
    * 使用PHP函数 **json_encode**
* php
    * 使用PHP函数 **serialize**
* dump
    * 使用PHP函数 **var_dump**
* xml
    * 待续
