# 常用命令
## 实用小工具
* base_func.sh
* json_decode.sh
* get_url_params.sh
* get_json_value.sh
* get_php_const.sh
* php_unserialize.sh
* set_json_value.sh
* timestamp_2_date.sh
* url_decode_lines.sh
* url_encode_lines.sh
* url_decode.sh
* url_encode.sh
* user_id_2_name.sh
* user_name_2_id.sh
* group_id_2_name.sh
* group_name_2_id.sh
* xpath.sh
* file_replace_str.sh
* beautify.sh

## 运维工具
* construct.sh
* create_oper_user.sh
* create_prog_user.sh
* apply_prog_users.sh
* get_oper_users.sh
* get_prog_users.sh
* start_service.sh
* stop_service.sh
* start_user.sh
* stop_user.sh
* cleanup_user.sh
* copy_ssh_key.sh

## 用户的pear实例管理工具
* reconstruct_user_pear.sh
* construct_user_pear.sh
* destroy_user_pear.sh

## 应用系统开发的使用工具
* init_system_code.sh

## API开发的实用工具
* verify_interface_definition.sh
* get_json_interface_definition.sh

## 性能测试
* performance_test.sh
