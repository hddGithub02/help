# 服务器安装规范

## CentOs 6.x

### 虚拟机(通过VmWare安装)

#### 安装操作系统

* 安装时的选项
    * 语言: English
    * 时区：中国的情况下，选择Asia/Shanghai
    * UTC for system clock： 不选中
    * 安装类型： Minimal
    * 用户： 只创建root用户
* 硬盘分区规则

    分区 | 文件系统类型 | 位置 | 大小 | 备注
    -----+--------------+------+------|---------
    swap | swap | 第1张盘 | 物理内存的2倍;最大值为2G | 
    /boot | ext4 | 第1张盘 | 128M | 
    /     | ext4 | 第1张盘 | 第1张硬盘的剩余所有空间 | 
    /da1     | ext4 | 第2张盘 | 第2张硬盘的所有空间 | 仅在有第2块硬盘的情况下存在
    /da2     | ext4 | 第3张盘 | 第3张硬盘的所有空间 | 仅在有第3块硬盘的情况下存在
    /daN     | ext4 | 第N+1张盘 | 第N+1张硬盘的所有空间 | 仅在有第N+1块硬盘的情况下存在
    
#### 配置网络

* 配置网卡，编辑文件/etc/sysconfig/network-scripts/ifcfg-eth0（如果网卡名称不是ifcfg-eth0，则文件名请替换成该网卡名称）

    ```
DEVICE=eth0
ONBOOT=yes
BOOTPROTO=static
IPADDR=192.168.0.120
    ```

    * IPADDR设置为分配给本虚拟机的IP地址，公司内部环境的网段：192.168.0.xxx

* 配置DNS服务器，编辑文件/etc/resolv.conf

    ```
nameserver 192.168.0.1
    ```
    
    * nameserver设置为本虚拟机所在环境的DNS服务器的IP地址，公司内部环境的DNS服务器为：192.168.0.1
    
* 配置主机名和缺省网关，编辑文件/etc/sysconfig/network

    ```
NETWORKING=yes
HOSTNAME=www.xcm03.com
GATEWAY=192.168.0.1
    ```

    * HOSTNAME设置为给本虚拟机分配的主机名
    * GATEWAY设置为本虚拟机所在环境的缺省网关的IP地址，公司内部环境的缺省网关IP为：192.168.0.1

* 重启网络服务

    ```
service network restart
    ```

* 确认网络是否设置成功

    ```
ping  www.baidu.com
    ```

##### 无互联网环境的服务器（如公安网内部的云主机）
* 拷贝rpm文件到服务器的/tmp/install/rpm目录下
* rpm安装

    ```
    cd /tmp/install/rpm
    ls | xargs -n 1 rmp -i --nodeps
    ```
    
#### 系统环境设置

* 拷贝下面的脚本内容，到本地文件/tmp/install_env.sh，并执行
    * [安装脚本](./scripts/setup_baseline_centos.sh)
    
    ```
vi /tmp/install_env.sh
...
chmod 755 /tmp/install_env.sh
/tmp/install_env.sh    #如果没有特殊原因，相关的[y/n]的选项，都请选择y
    ```
    
#### 安装运维库(Pear包)
* 下载如下的Pear的相关代码

    ```
git clone 'http://$user@58.60.110.105:8000/lib/pear.git' /var/pear        ; $user请使用自己的git用户名
    ```

* 安装全局所使用的Pear包

    ```
pear channel-add /var/pear/src/pear.taxicolor.org/channel.xml
pear install /var/pear/src/pear.taxicolor.org/package/utility/1.0.0/package.xml
pear install /var/pear/src/pear.taxicolor.org/package/info/1.0.0/package.xml
pear install /var/pear/src/pear.taxicolor.org/package/maint/1.0.0/package.xml
pear install /var/pear/src/pear.taxicolor.org/package/pear_mgr/1.0.0/package.xml
    ```
